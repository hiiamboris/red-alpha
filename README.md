This is a sample to demo how to use transparent windows in Red 0.6.4.

How to use:
- clone this repo
- `red -r -t Windows red-alpha.red` to compile it
- run the exe & have fun dragging the pic around

Sample saturn image is fetched from here: https://space-facts.com/transparent-planet-pictures/